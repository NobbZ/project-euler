defmodule Euler.Problem7Test do
  use ExUnit.Case

  test "10_001st prime number is 104_743" do
    assert 104_743 = Euler.Problem7.nth_prime(10_001)
  end

  test "list of primes is correct over the first items" do
    assert [2, 3, 5, 7, 11, 13, 17, 19, 23, 29] = Euler.Problem7.primes() |> Enum.take(10)
  end
end
