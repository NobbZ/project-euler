defmodule Euler.Problem7 do
  def nth_prime(n) do
    primes()
    |> Stream.drop(n - 1)
    |> Enum.take(1)
    |> hd()
  end

  def primes() do
    seed_primes = [2, 3]

    calculated_primes =
      Stream.iterate(1, &(&1 + 1))
      |> Stream.flat_map(fn x ->
        x = 6 * x
        [x - 1, x + 1]
      end)
      |> Stream.transform(seed_primes, fn c, ps ->
        if Enum.any?(ps, fn p -> rem(c, p) == 0 end) do
          {[], ps}
        else
          {[c], [c | ps]}
        end
      end)

    Stream.concat(seed_primes, calculated_primes)
  end
end
