#!/usr/bin/env bats

@test "Largest product in a series" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "23514624000" ]
}
