open! Base

let check candidate n ps =
  let rec go = function
    | p::_ when candidate % p = 0 -> ps, n
    | _::ps' -> go ps'
    | [] -> candidate::ps, (n - 1) in
  go ps

let nth_prime n =
  let rec go ps c1 c2 = function
    | 0 -> List.hd_exn ps
    | n -> let ps', n' = check c1 n ps in
      let ps'', n'' = check c2 n' ps' in
      go ps'' (c1 + 6) (c2 + 6) n'' in
  match n with
  | 1 -> 2
  | 2 -> 3
  | _ -> go [2; 3] 5 7 (n - 2)

let () =
  nth_prime 10001
  |> Stdio.printf "%i\n"