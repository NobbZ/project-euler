#!/usr/bin/env bats

@test "10001st prime" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "104743" ]
}
