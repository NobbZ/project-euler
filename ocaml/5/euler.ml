open! Base

let numbers =
  let rec go n nums =
    match n with
    | 0 -> nums
    | _ -> go (n - 1) (n::nums) in
  go 20 []

let lcm xs =
  let rec gcd a = function
    | 0 -> a
    | b -> gcd b (a % b) in
  let rec lcm' x = function
    | y::ys -> lcm' (abs (x * y) / gcd x y) ys
    | [] -> x in
  match xs with
  | x::xs' -> lcm' x xs
  | [] -> failwith "empty list"

let () =
  numbers
  |> lcm
  |> Stdio.printf "%i\n"