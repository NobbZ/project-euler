#!/usr/bin/env bats

@test "Smallest multiple" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "232792560" ]
}
