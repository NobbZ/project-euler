#!/usr/bin/env bats

@test "Large sum" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "5537376230" ]
}
