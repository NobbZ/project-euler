open! Base

let collatz n =
  let rec go seq = function
    | 1 -> List.rev (1::seq)
    | n when n % 2 = 0 -> go (n::seq) (n / 2)
    | n -> go (n::seq) (n * 3 + 1) in
  go [] n

let () =
  Sequence.range 1 1_000_000
  |> Sequence.map ~f:collatz
  |> Sequence.fold ~init:[] ~f:(fun max s -> if List.length s > List.length max then s else max)
  |> List.hd_exn
  |> Stdio.printf "%i\n"