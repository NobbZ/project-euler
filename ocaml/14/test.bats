#!/usr/bin/env bats

@test "Longest Collatz sequence" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "837799" ]
}
