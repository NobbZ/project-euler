open! Base

let square_of_sums n =
  let sum = (n * n + n) / 2 in
  sum * sum

let sum_of_squares n =
  (n * (n + 1) * (2 * n + 1)) / 6

let () =
  square_of_sums 100 - sum_of_squares 100
  |> Stdio.printf "%i\n"