#!/usr/bin/env bats

@test "Sum square difference" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "25164150" ]
}
