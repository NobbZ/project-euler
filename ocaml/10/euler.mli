open! Base

val is_prime : Int.t -> bool

val primes : Int.t Sequence.t

val id : 'a -> 'a