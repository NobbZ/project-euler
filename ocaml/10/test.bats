#!/usr/bin/env bats

@test "Summation of primes" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "142913828922" ]
}
