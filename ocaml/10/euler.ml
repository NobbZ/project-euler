open! Base

let is_prime c =
  let rec go n = if n * n > c then true else if c % n = 0 then false else go (n + 1) in
  go 2

let primes =
  let open Sequence.Step in
  Sequence.unfold_step ~init:2 ~f:(function
    | 2 -> Yield (2, 3)
    | c when is_prime c -> Yield (c, c + 2)
    | c -> Skip (c + 2))

let id x = x

let () =
  primes
  |> Sequence.take_while ~f:(fun p -> p < 2_000_000)
  |> Sequence.sum (module Int) ~f:(id)
  |> Stdio.printf "%i\n"