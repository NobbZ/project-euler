#!/usr/bin/env bats

@test "Even Fibonacci numbers" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "906609" ]
}
