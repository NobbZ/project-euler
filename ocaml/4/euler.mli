open! Base

val is_palindrome : String.t -> bool

val is_palindrome_num : Int.t -> bool

val search : Int.t -> Int.t -> Int.t -> Int.t