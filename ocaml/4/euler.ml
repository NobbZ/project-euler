open! Base

let is_palindrome s =
  String.(s = String.rev s)

let is_palindrome_num n =
  n
  |> Int.to_string
  |> is_palindrome

let rec search n m biggest =
  let num = n * m in
  let biggest' = if is_palindrome_num num && num > biggest then num else biggest in
  match n < 100, m < 100 with
  | true, _ -> biggest'
  | _, true -> search (n - 1) (n - 1) biggest'
  | _, _ -> search n (m - 1) biggest'

let () =
  search 999 999 0
  |> Stdio.printf "%i\n"