#!/usr/bin/env bats

@test "Largest product in a grid" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "70600674" ]
}
