open! Base

type t

type dir = Hor | Vert | DiagR | DiagL

type pos = (Int.t * Int.t)

val grid : t

val at : pos -> t -> Int.t

val from : pos -> dir -> Int.t -> t -> Int.t List.t Option.t 