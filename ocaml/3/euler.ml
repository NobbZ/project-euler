open! Base

let prime_factors n =
  let rec pf' n m fs =
    match m > n, n % m with
    | true, _ -> fs
    | _, 0 -> pf' (n / m) m (m::fs)
    | _, _ -> pf' n (m + 2) fs in
  let rec pf n fs =
    if n % 2 = 0
    then pf (n / 2) (2::fs)
    else pf' n 3 fs in
  pf n []

let () =
  Stdio.printf "%i\n" (600_851_475_143 |> prime_factors |> List.hd_exn)