open! Base

val triangle_numbers : Int.t Sequence.t

val divisors : Int.t -> Int.t
