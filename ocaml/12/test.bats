#!/usr/bin/env bats

@test "Highly divisible triangular number" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "76576500" ]
}
