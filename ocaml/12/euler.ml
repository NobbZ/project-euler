open! Base

let triangle_numbers =
  Sequence.unfold ~init:(1, 0) ~f:(fun (n, s) -> let s' = s + n in Some (s', (n + 1, s')))

let divisors = function
  | 1 -> 1
  | n -> Sequence.unfold_step ~init:1 ~f:(function
      | m when m * m > n -> Sequence.Step.Done
      | m when n % m = 0 -> Sequence.Step.Yield (m, m + 1)
      | m -> Sequence.Step.Skip (m + 1))
    |> Sequence.length
    |> (fun x -> 2 * x)

let () =
  triangle_numbers
  |> Sequence.filter ~f:(fun n -> divisors n > 500)
  |> Sequence.hd_exn
  |> Stdio.printf "%i\n"