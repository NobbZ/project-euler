open! Base

let rec sum_even_fibs_below_four_million =
  let rec go a b c sum =
    if c >= 4_000_000
      then sum
      else go (b + c) (2 * c + b) (2 * b + 3 * c) (sum + c) in
  go 1 1 2 0

let () =
  Stdio.printf "%i\n" sum_even_fibs_below_four_million