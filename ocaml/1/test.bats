#!/usr/bin/env bats

@test "Sum of multiples of 3 and 5 below 1000" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "233168" ]
}
