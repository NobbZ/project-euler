open! Base

let sum_of_multiples n up_to =
  let m = up_to / n in
  n * (m * m + m) / 2

let () =
  let result = sum_of_multiples 3 999 + sum_of_multiples 5 999 - sum_of_multiples 15 999 in
  Stdio.printf "%i\n" result