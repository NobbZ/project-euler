open! Base

type triplet = Int.t * Int.t * Int.t

let triplets =
  let stepper (a, b, c) =
    match (a, b, c) with
    | 1001, 1001, 1001 -> Sequence.Step.Done
    | a, 1001, _ -> Sequence.Step.Skip (a + 1, 1, 1)
    | a, b, 1001 -> Sequence.Step.Skip (a, b + 1, 1)
    | a, b, c -> Sequence.Step.Yield ((a, b, c), (a, b, c + 1)) in
  Sequence.unfold_step ~init:(1, 1, 1) ~f:stepper

let () =
  triplets
  |> Sequence.filter ~f:(fun (a, b, c) -> a * a + b * b = c * c)
  |> Sequence.filter ~f:(fun (a, b, c) -> a + b + c = 1000)
  |> Sequence.map ~f:(fun (a, b, c) -> a * b * c)
  |> Sequence.hd_exn
  |> Stdio.printf "%i\n"