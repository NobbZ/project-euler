open! Base

type triplet = Int.t * Int.t * Int.t

val triplets : triplet Sequence.t