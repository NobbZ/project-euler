#!/usr/bin/env bats

@test "Special Pythagorean triplet" {
    result="$(_build/default/euler.exe)"
    [ "$result" = "31875000" ]
}
