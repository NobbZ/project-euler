LANGUAGES = $(sort $(shell find . -mindepth 1 -maxdepth 1 -regextype posix-extended -regex './[[:alnum:]]+' -type d))

test: ${LANGUAGES:%=%_test}

clean: ${LANGUAGES:%=%_clean}

%_test:
	$(MAKE) -C $* test

%_clean:
	$(MAKE) -C $* clean