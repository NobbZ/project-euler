extern crate num_integer;

use num_integer::Roots;

fn main() {
    let result = (1 as u64..)
        .map(|n| n * (n + 1) / 2)
        .map(|n| {
            (n, {
                let mut cnt: u64 = 0;
                for m in 1..(n.sqrt() + 1) {
                    if n % m == 0 {
                        cnt += 2
                    };
                }
                cnt
            })
        })
        .filter(|&(_, num_divs)| num_divs > 500)
        .take(1)
        .collect::<Vec<_>>();

    println!("{:?}", result[0].0)
}
