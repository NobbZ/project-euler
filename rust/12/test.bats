#!/usr/bin/env bats

@test "Highly divisible triangular number" {
    result="$(cargo run --release)"
    [ "$result" = "76576500" ]
}
